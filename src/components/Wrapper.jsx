import { HStack, VStack } from "@chakra-ui/react";
import Navbar from "./Navbar";

function Wrapper(props) {
  return (
    <VStack minH='100vh' minW='100vw'>
      <Navbar />
      <HStack>{props.children}</HStack>
    </VStack>
  );
}

export default Wrapper;
